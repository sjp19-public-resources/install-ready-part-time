# Custom Ansible Playbook for Ubuntu on WSL2 and macOS

This is a custom ansible playbook. It sets up all the tools for local web
development using Node.js

## How to use this

### Windows

1. Install WSL2.
2. Install Ubuntu (22.04 LTS recommended)
3. Run the playbook

    ```shell
    curl https://gitlab.com/sjp19-public-resources/install-ready-sjp2-installer/-/raw/main/install.sh | bash
    ```

## MacOS

1. Install Command Line Developer Tools

    ```shell
    xcode-select --install
    ```

1. Run the playbook

    ```shell
    curl https://gitlab.com/sjp19-public-resources/install-ready-sjp2-installer/-/raw/main/install.sh | bash
    ```

This installs the following things:

- zsh (WSL Only)
- acl (WSL Only)
- git (WSL Only)
- homebrew (Mac Only)
- fnm (Fast Node Mananger)
- Node.js (You can change the version in the playbook.yml file)
- Generates an ssh key, it will be in ~/.ssh/id_ed25519.pub
- Sets VSCODE to be your default editor.
- pyenv and python 3.13
- Sets up an `open` and `wsl` alias (WSL Only)
