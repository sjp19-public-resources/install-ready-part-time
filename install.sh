#!/bin/bash
#
function blue {
    printf "\e[34m$1\e[0m\n"
}

function green {
    printf "\e[36m$1\e[0m\n"
}

# Remove the old install ready if it exists
if [ -d "${HOME}/.install-ready" ]; then
    blue "-----------------------------------------------------"
    blue "Removing old install ready folder so we can try again"
    blue "-----------------------------------------------------"
    rm -rf "${HOME}/.install-ready"
fi

# Downloading install ready
green "----------------------------"
green "Downloading install ready..."
green "----------------------------"
git clone https://gitlab.com/sjp19-public-resources/install-ready-sjp2-installer.git "${HOME}/.install-ready"
cd "$HOME/.install-ready"

if [ $(uname -s) == 'Linux' ]; then
    blue "-------------------"
    blue "Running on Linux..."
    blue "-------------------"
    blue "-------------------------"
    blue "Enter your sudo password:"
    blue "-------------------------"
    sudo echo
    blue "-------------------------------------"
    blue "Updating Operating System Software..."
    blue "-------------------------------------"
    sudo apt update -q
    if [ $(lsb_release -r -s) == "20.04" ]; then
        sudo apt install -q -y python3 python3-pip python3-venv
    else
        sudo apt install -q -y python3-full
    fi
fi

if [ $(uname -s) == 'Darwin' ]; then
    blue "-------------------"
    blue "Running on macOS..."
    blue "-------------------"
fi

# Creating a virtual environment to do the python work
blue "--------------------------------------"
blue "Setting up python virtual environment"
blue "--------------------------------------"
python3 -m venv .venv
source .venv/bin/activate

# Upgrade pip
blue "---------------------"
blue "Upgrading pip"
blue "---------------------"
python -m pip install --upgrade pip

# Install ansible with pip
blue "---------------------"
blue "Installing Ansible..."
blue "---------------------"
python -m pip install ansible

# Install third party ansible roles
blue "---------------------------"
blue "Installing Ansible roles..."
blue "---------------------------"
ansible-galaxy install --ignore-certs -r requirements.yml

# Run playbook
blue "---------------------------"
blue "Running Ansible Playbook..."
blue "Enter your password"
echo "-------------------"

python3 -m ansible playbook -K playbook.yml
blue "-----------------------------------------"
blue "Cleaning up python virtual environment..."
blue "-----------------------------------------"
deactivate
rm -rf .venv

blue "----------------------------"
blue "Cleaning up install ready..."
blue "----------------------------"
cd "$HOME"
rm -rf ~/.install-ready

green "------------------------------------"
green " Ansible Install Complete! Congrats!"
green "------------------------------------"
echo ""

blue "----------------------------------------------"
blue "This is the public SSH key that was generated."
blue "Save this someplace before closing this window"
blue "You will need it to add to gitlab."
blue "----------------------------------------------"
echo ""
cat ~/.ssh/id_ed25519.pub
green "----------------------------------------------------"
green "Once you have saved your SSH key,"
green "close this terminal and open a new one."
green "----------------------------------------------------"
